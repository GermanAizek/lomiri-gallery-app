# Spanish translation for gallery-app
# Copyright (c) 2013 Rosetta Contributors and Canonical Ltd. 2013
# This file is distributed under the same license as the gallery-app package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2013.
#
msgid ""
msgstr ""
"Project-Id-Version: gallery-app\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-10-07 12:10+0000\n"
"PO-Revision-Date: 2023-11-15 18:05+0000\n"
"Last-Translator: gallegonovato <fran-carro@hotmail.es>\n"
"Language-Team: Spanish <https://hosted.weblate.org/projects/lomiri/"
"lomiri-gallery-app/es/>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 5.2-dev\n"
"X-Launchpad-Export-Date: 2017-04-05 07:16+0000\n"

#: lomiri-gallery-app.desktop.in:4 rc/qml/MediaViewer/PopupPhotoViewer.qml:46
msgid "Gallery"
msgstr "Galería"

#: lomiri-gallery-app.desktop.in:5
msgid "Photo Viewer"
msgstr "Visor de fotos"

#: lomiri-gallery-app.desktop.in:6
msgid "Browse your photographs"
msgstr "Explore sus fotos"

#: lomiri-gallery-app.desktop.in:7
msgid "Photographs;Pictures;Albums"
msgstr "Fotografías;Imágenes;Álbumes"

#: lomiri-gallery-app.desktop.in:9
msgid "lomiri-gallery-app"
msgstr ""

#: rc/qml/AlbumEditor/AlbumEditMenu.qml:43
#: rc/qml/AlbumEditor/AlbumEditor.qml:36
msgid "Edit album"
msgstr "Editar álbum"

#: rc/qml/AlbumEditor/AlbumEditMenu.qml:50
#: rc/qml/AlbumViewer/AlbumViewer.qml:378 rc/qml/Components/DeleteDialog.qml:32
#: rc/qml/Components/DeleteDialog.qml:36 rc/qml/EventsOverview.qml:120
#: rc/qml/MediaViewer/MediaViewer.qml:226
#: rc/qml/MediaViewer/MediaViewer.qml:351 rc/qml/PhotosOverview.qml:193
#: rc/qml/Utility/EditingHUD.qml:66
msgid "Delete"
msgstr "Eliminar"

#: rc/qml/AlbumEditor/AlbumEditor.qml:39 rc/qml/Components/DeleteDialog.qml:45
#: rc/qml/Components/MediaSelector.qml:93 rc/qml/EventsOverview.qml:166
#: rc/qml/MediaViewer/MediaViewer.qml:238
#: rc/qml/MediaViewer/MediaViewer.qml:279 rc/qml/PhotosOverview.qml:240
#: rc/qml/PickerScreen.qml:289
msgid "Cancel"
msgstr "Cancelar"

#: rc/qml/AlbumViewer/AlbumCoverList.qml:113
msgid "Default"
msgstr "Por defecto"

#: rc/qml/AlbumViewer/AlbumCoverList.qml:114
msgid "Blue"
msgstr "Azul"

#: rc/qml/AlbumViewer/AlbumCoverList.qml:115
msgid "Green"
msgstr "Verde"

#: rc/qml/AlbumViewer/AlbumCoverList.qml:116
msgid "Pattern"
msgstr "Patrón"

#: rc/qml/AlbumViewer/AlbumCoverList.qml:117
msgid "Red"
msgstr "Rojo"

#: rc/qml/AlbumViewer/AlbumViewer.qml:107 rc/qml/MainScreen.qml:66
msgid "Album"
msgstr "Álbum"

#: rc/qml/AlbumViewer/AlbumViewer.qml:372
#: rc/qml/MediaViewer/MediaViewer.qml:341
msgid "Add to album"
msgstr "Añadir al álbum"

#: rc/qml/AlbumsOverview.qml:229 rc/qml/Components/PopupAlbumPicker.qml:91
msgid "Add new album"
msgstr "Añadir álbum nuevo"

#: rc/qml/AlbumsOverview.qml:233 rc/qml/Components/PopupAlbumPicker.qml:101
msgid "New Photo Album"
msgstr "Álbum de fotos nuevo"

#: rc/qml/AlbumsOverview.qml:234 rc/qml/Components/PopupAlbumPicker.qml:102
msgid "Subtitle"
msgstr "Subtítulo"

#: rc/qml/AlbumsOverview.qml:241 rc/qml/EventsOverview.qml:109
#: rc/qml/PhotosOverview.qml:182
msgid "Camera"
msgstr "Cámara"

#: rc/qml/Components/DeleteOrDeleteWithContentsDialog.qml:51
msgid "Delete album"
msgstr "Eliminar álbum"

#: rc/qml/Components/DeleteOrDeleteWithContentsDialog.qml:60
msgid "Delete album AND contents"
msgstr "Eliminar álbum Y su contenido"

#: rc/qml/Components/EventCard.qml:59
msgid "MMM yyyy"
msgstr "MMM yyyy"

#: rc/qml/Components/EventCard.qml:74
msgid "dd"
msgstr "dd"

#: rc/qml/Components/MediaSelector.qml:59
#: rc/qml/Components/MediaSelector.qml:81
msgid "Add to Album"
msgstr "Añadir al álbum"

#: rc/qml/Components/PopupAlbumPicker.qml:162 rc/qml/Utility/EditingHUD.qml:77
msgid "Add Photo to Album"
msgstr "Añadir foto al álbum"

#: rc/qml/Components/UnableShareDialog.qml:25
msgid "Unable to share"
msgstr "No se puede compartir"

#: rc/qml/Components/UnableShareDialog.qml:26
msgid "Unable to share photos and videos at the same time"
msgstr "Imposible compartir fotos y vídeos al mismo tiempo"

#: rc/qml/Components/UnableShareDialog.qml:30
msgid "Ok"
msgstr "Aceptar"

#: rc/qml/EventsOverview.qml:80 rc/qml/PhotosOverview.qml:103
#, qt-format
msgid "Delete %1 photo"
msgid_plural "Delete %1 photos"
msgstr[0] "Eliminar %1 foto"
msgstr[1] "Eliminar %1 fotos"

#: rc/qml/EventsOverview.qml:86 rc/qml/PhotosOverview.qml:109
#, qt-format
msgid "Delete %1 video"
msgid_plural "Delete %1 videos"
msgstr[0] "Eliminar %1 vídeo"
msgstr[1] "Eliminar %1 vídeos"

#: rc/qml/EventsOverview.qml:92 rc/qml/PhotosOverview.qml:115
#, qt-format
msgid "Delete %1 media file"
msgid_plural "Delete %1 media files"
msgstr[0] "Eliminar %1 archivo multimedia"
msgstr[1] "Eliminar %1 archivos multimedia"

#: rc/qml/EventsOverview.qml:127 rc/qml/MediaViewer/MediaViewer.qml:362
#: rc/qml/PhotosOverview.qml:200 rc/qml/Utility/EditingHUD.qml:71
msgid "Share"
msgstr "Compartir"

#: rc/qml/EventsOverview.qml:141 rc/qml/PhotosOverview.qml:215
msgid "Unselect all"
msgstr "Deselecciona todo"

#: rc/qml/EventsOverview.qml:141 rc/qml/PhotosOverview.qml:215
msgid "Select all"
msgstr "Selecciona todo"

#: rc/qml/EventsOverview.qml:156 rc/qml/PhotosOverview.qml:230
#: rc/qml/Utility/EditingHUD.qml:76
msgid "Add"
msgstr "Añadir"

#: rc/qml/EventsOverview.qml:208 rc/qml/MediaViewer/MediaViewer.qml:182
#: rc/qml/PhotosOverview.qml:259
msgid "Share to"
msgstr "Compartir con"

#: rc/qml/LoadingScreen.qml:39
msgid "Loading…"
msgstr "Cargando…"

#: rc/qml/MainScreen.qml:152
msgid "Albums"
msgstr "Álbumes"

#: rc/qml/MainScreen.qml:166 rc/qml/MainScreen.qml:205
#: rc/qml/PickerScreen.qml:157
msgid "Events"
msgstr "Eventos"

#: rc/qml/MainScreen.qml:203 rc/qml/MainScreen.qml:255
#: rc/qml/PickerScreen.qml:195 rc/qml/PickerScreen.qml:243
msgid "Select"
msgstr "Seleccionar"

#: rc/qml/MainScreen.qml:214 rc/qml/MainScreen.qml:257
#: rc/qml/PickerScreen.qml:206
msgid "Photos"
msgstr "Fotos"

#: rc/qml/MediaViewer/MediaViewer.qml:215
msgid "Delete a photo"
msgstr "Eliminar una foto"

#: rc/qml/MediaViewer/MediaViewer.qml:215
msgid "Delete a video"
msgstr "Eliminar un vídeo"

#: rc/qml/MediaViewer/MediaViewer.qml:249
msgid "Remove a photo from album"
msgstr "Eliminar una foto del álbum"

#: rc/qml/MediaViewer/MediaViewer.qml:249
msgid "Remove a video from album"
msgstr "Eliminar un vídeo del álbum"

#: rc/qml/MediaViewer/MediaViewer.qml:258
msgid "Remove from Album"
msgstr "Eliminar del álbum"

#: rc/qml/MediaViewer/MediaViewer.qml:269
msgid "Remove from Album and Delete"
msgstr "Quitar del álbum y eliminar"

#: rc/qml/MediaViewer/MediaViewer.qml:316
msgid "Edit"
msgstr "Editar"

#: rc/qml/MediaViewer/MediaViewer.qml:372
msgid "Info"
msgstr "Información"

#: rc/qml/MediaViewer/MediaViewer.qml:395
msgid "Information"
msgstr "Información"

#: rc/qml/MediaViewer/MediaViewer.qml:398
msgid "Media type: "
msgstr "Tipo de medio: "

#: rc/qml/MediaViewer/MediaViewer.qml:399
msgid "photo"
msgstr "foto"

#: rc/qml/MediaViewer/MediaViewer.qml:399
msgid "video"
msgstr "vídeo"

#: rc/qml/MediaViewer/MediaViewer.qml:400
msgid "Media name: "
msgstr "Nombre del medio: "

#: rc/qml/MediaViewer/MediaViewer.qml:401
msgid "Date: "
msgstr "Fecha: "

#: rc/qml/MediaViewer/MediaViewer.qml:402
msgid "Time: "
msgstr "Hora: "

#: rc/qml/MediaViewer/MediaViewer.qml:408
msgid "ok"
msgstr "aceptar"

#: rc/qml/MediaViewer/PhotoEditorPage.qml:26
msgid "Edit Photo"
msgstr "Editar foto"

#: rc/qml/MediaViewer/PopupPhotoViewer.qml:183
msgid "Toggle Selection"
msgstr "Invertir selección"

#: rc/qml/MediaViewer/SingleMediaViewer.qml:241
msgid "An error has occurred attempting to load media"
msgstr "Ocurrió un error intentando cargar el contenido"

#: rc/qml/PhotosOverview.qml:132 rc/qml/PhotosOverview.qml:175
msgid "Grid Size"
msgstr "Tamaño de la cuadrícula"

#: rc/qml/PhotosOverview.qml:133
msgid "Select the grid size in gu units between 8 and 20 (default is 12)"
msgstr ""
"Seleccionar el tamaño de la cuadrícula en unidades «gu» entre 8 y 20 "
"(predeterminado: 12)"

#: rc/qml/PhotosOverview.qml:148
msgid "Finished"
msgstr "Finalizado"

#: rc/qml/PickerScreen.qml:295
msgid "Pick"
msgstr "Escoger"

#: rc/qml/Utility/EditingHUD.qml:67
msgid "Trash;Erase"
msgstr "Papelera;Borrar"

#: rc/qml/Utility/EditingHUD.qml:72
msgid "Post;Upload;Attach"
msgstr "Publicar;Subir;Adjuntar"

#: rc/qml/Utility/EditingHUD.qml:81
msgid "Undo"
msgstr "Deshacer"

#: rc/qml/Utility/EditingHUD.qml:82
msgid "Cancel Action;Backstep"
msgstr "Cancelar acción;Paso atrás"

#: rc/qml/Utility/EditingHUD.qml:86
msgid "Redo"
msgstr "Rehacer"

#: rc/qml/Utility/EditingHUD.qml:87
msgid "Reapply;Make Again"
msgstr "Reaplicar;De nuevo"

#: rc/qml/Utility/EditingHUD.qml:91
msgid "Auto Enhance"
msgstr "Mejora automática"

#: rc/qml/Utility/EditingHUD.qml:92
msgid "Adjust the image automatically"
msgstr "Ajustar la imagen automáticamente"

#: rc/qml/Utility/EditingHUD.qml:93
msgid "Automatically Adjust Photo"
msgstr "Ajustar foto automáticamente"

#: rc/qml/Utility/EditingHUD.qml:98
msgid "Rotate"
msgstr "Rotar"

#: rc/qml/Utility/EditingHUD.qml:99
msgid "Turn Clockwise"
msgstr "Girar en sentido horario"

#: rc/qml/Utility/EditingHUD.qml:100
msgid "Rotate the image clockwise"
msgstr "Rota la imagen en sentido horario"

#: rc/qml/Utility/EditingHUD.qml:105
msgid "Crop"
msgstr "Recortar"

#: rc/qml/Utility/EditingHUD.qml:106
msgid "Trim;Cut"
msgstr "Recortar;Cortar"

#: rc/qml/Utility/EditingHUD.qml:107
msgid "Crop the image"
msgstr "Recortar la imagen"

#: rc/qml/Utility/EditingHUD.qml:112
msgid "Revert to Original"
msgstr "Revertir al original"

#: rc/qml/Utility/EditingHUD.qml:113
msgid "Discard Changes"
msgstr "Descartar cambios"

#: rc/qml/Utility/EditingHUD.qml:114
msgid "Discard all changes"
msgstr "Descartar todos los cambios"

#: rc/qml/Utility/EditingHUD.qml:118
msgid "Exposure"
msgstr "Exposición"

#: rc/qml/Utility/EditingHUD.qml:119
msgid "Adjust the exposure"
msgstr "Ajustar exposición"

#: rc/qml/Utility/EditingHUD.qml:120
msgid "Underexposed;Overexposed"
msgstr "Subexpuesta;Sobreexpuesta"

#: rc/qml/Utility/EditingHUD.qml:121 rc/qml/Utility/EditingHUD.qml:162
msgid "Confirm"
msgstr "Confirmar"

#: rc/qml/Utility/EditingHUD.qml:125
msgid "Compensation"
msgstr "Compensación"

#: rc/qml/Utility/EditingHUD.qml:159
msgid "Color Balance"
msgstr "Balance de colores"

#: rc/qml/Utility/EditingHUD.qml:160
msgid "Adjust color balance"
msgstr "Ajustar el balance de colores"

#: rc/qml/Utility/EditingHUD.qml:161
msgid "Saturation;Hue"
msgstr "Saturación;Matiz"

#: rc/qml/Utility/EditingHUD.qml:166
msgid "Brightness"
msgstr "Brillo"

#: rc/qml/Utility/EditingHUD.qml:177
msgid "Contrast"
msgstr "Contraste"

#: rc/qml/Utility/EditingHUD.qml:188
msgid "Saturation"
msgstr "Saturación"

#: rc/qml/Utility/EditingHUD.qml:199
msgid "Hue"
msgstr "Matiz"

#~ msgid "Yes"
#~ msgstr "Sí"

#~ msgid "No"
#~ msgstr "No"

#~ msgid "Delete 1 photo"
#~ msgstr "Eliminar una foto"

#~ msgid "Delete 1 video"
#~ msgstr "Eliminar un vídeo"

#~ msgid "Delete %1 photos and 1 video"
#~ msgstr "Eliminar %1 fotos y un vídeo"

#~ msgid "Delete 1 photo and %1 videos"
#~ msgstr "Eliminar una foto y %1 vídeos"

#~ msgid "Delete 1 photo and 1 video"
#~ msgstr "Eliminar una foto y un vídeo"

#~ msgid "Delete %1 photos and %2 videos"
#~ msgstr "Eliminar %1 fotos y %2 vídeos"
